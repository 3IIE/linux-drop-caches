"""
This file is to show and manage dentries, inodes, and page caches/memory on
linux operating systems.

Useful links:

https://stackoverflow.com/questions/29870068/what-are-pagecache-dentries-inodes
https://en.wikipedia.org/wiki/Page_cache
https://linuxize.com/post/cron-jobs-every-5-10-15-minutes/

Inspired by:  https://lemmy.ml/post/14918974

Usage:

  Help:

    -h, --help     |  Show this info

  Checking memory flag:

    -s, --show     |  Passed in args will print useful memory info. Can be
                   |  passed with --clear flag to show info before and after
                   |  caches are cleared.

  Clearing memory flags and keyword arguments:

    -c, --clear    |   Will activate the clearing of caches. Everything further
                       indented below will require this flag to operate.

      -pc, --pages |   Will clear pages cache.

      -id, --inode |   Will clear the dentries and inodes cache.

      -a, --all    |   Will clear all the caches. When passed the two above
                       flags are not needed.

        -sysd      |   Will use systemd to clear the caches. If passed the 
                   |   above 3 flags will use systemd to clear caches. If not
                   |   passed program will use non systemd method.

      Keyword arguments:

        percent    |   Any number between 1 and 100. Will only clear the caches
                   |   when the total used memory percentage is greater than
                   |   user defined percentage defined by this argument.
                   |   Default is 99.

        apps       |   A list of apps separated by commas without spaces. The
                   |   program will look through all running processes to find
                   |   any process in the list. If found and the desired memory
                   |   percentage is exceeded the desired caches will be
                   |   cleared.

  Examples:

    Show info:
        python memoryfix.py --show

    Show info, clear all caches using systemd:
        python memoryfix.py -s --clear -all -sysd

    Clear dentries and inodes caches when default memory value has been
    exceeded and the users specified applications have been found to be running
    on the system:

        python memoryfix.py -c -id apps=chrome,blender,vlc

    Clear page cache when used memory exceeds 87%:
        python memoryfix.py -c percent=87

            **Note: remember there must not be any spaces in keywords,
                    separators, and arguments.

  Cronjob setup for every five minutes and clear cache only if used memory is
  above 95 percent:

    Download file. Move it to wherever you want. My case ~/.scripts .
    Make file executable:
        chmod +x memoryfix.py

    For security might want to make file owned by root so it cant be edited.
        sudo chown root:root memoryfix.py

    Set the crontab:
        sudo EDITOR=nano crontab -e

    */5 * * * * /usr/bin/python /home/$USER/.scripts/memory.py -c percent=95

    Place the above line in the nano editor replacing the $USER with your
    username and adjust for where you have the file located. Now save the 
    crontab:
              [Ctrl] + o
              [Ctrl] + m
              [Ctrl] + x

"""
from sys import argv, exit as sExit
from subprocess import Popen, PIPE
from psutil import virtual_memory as vm, process_iter as ps

class Memory():
    """A class to show and manage dentries, inodes, and page caches/memory on
linux operating systems.

 Usage:

  Checking memory flag:

    -s, --show     |  Passed in args will print useful memory info.

  Clearing memory flags and keyword arguments:

    -c, --clear    |   Will activate the clearing of caches. Everything further
                       indented below will require this flag to operate.

      -pc, --pages |   Will clear pages cache.

      -id, --inode |   Will clear the dentries and inodes cache.

      -a, --all    |   Will clear all the caches. When passed the two above
                       flags are not needed.

        -sysd      |   Will use systemd to clear the caches. If passed the 
                   |   above 3 flags will use systemd to clear caches. If not
                   |   passed program will use non systemd method.

      Keyword arguments:

        percent    |   Any number between 1 and 100. Will only clear the caches
                   |   when the total used memory percentage is greater than
                   |   user defined percentage defined by this argument.
                   |   Default is 99.

        apps       |   A list of apps. The program will look through all 
                   |   running processes to find any process in the list. If
                   |   found and the desired memory percentage is exceeded the
                   |   desired caches will be cleared.

            Example: Memory(*['-c', '--all''], **{'percent': 85})

  """
    def __init__(self, *arg, **karg):
        """ """
        ## total memory
        self.memory = vm()

        self.cache_info(arg)

        ## ask if user wants to clear the memory
        if 1 in (1 for x in arg if x in ('-c', '--clear')):

            ## percent user specifies to trigger cache drop. Default is 99%
            self.percent = 99
            self.user_percent(karg)

            if (self.memory.percent > self.percent and
                self.app_finder(arg, karg)):
                ## memory is greater than desired memory percentage and found
                ## an app or two.
                self.clear_cache(arg)

            if 'apps' not in karg and self.memory.percent > self.percent:
                ## memory is greater than desired memory percentage
                self.clear_cache(arg)

            if 'percent' not in karg and 'apps' not in karg:
                ## force clear memory
                self.clear_cache(arg)
                print('here')


    def app_finder(self, arg, karg)->bool:
        """Look for specific apps in all running processes on the system and
return True if found and False if not."""
        if 'apps' in karg:
            apps = karg['apps']

            if ',' in apps:
                apps = apps.split(',')
            else:
                apps = [apps]

            found = [x for x in apps if x in (x.name() for x in ps())]
            if 1 in (1 for x in arg if x in ('-s', '--show')):
                print(f"Peocesses found: {', '.join(found)}")
            if found:
                return True
            return False
        return False


    def user_percent(self, karg):
        """Check to see if user wants a specific percentage of memory"""
        if 1 in [1 for x in karg if 'percent' in x]:
            ## user want's more control over when their memory cache is
            ## dropped
            try:
                self.percent = int(karg['percent'])
            except ValueError as e:
                err_str = 'The percent argument must be a number 1-100.'
                err_str += 'Example: percent=50'
                sExit(f"{e}\n   {err_str}")

            ## lets just make sure that the user passed an exceptable
            ## number
            if not self.percent > 1 or not self.percent < 100:
                exit_str = 'The percentage must be greater than 1 and less'
                exit_str += ' than 100.'
                sExit(exit_str)


    def page_cache(self, arg):
        """Method returns a list of commands to drop page cache using
systemd"""
        if '-sysd' in arg:
            ## user wants to use systemd
            return ['sudo', 'sysctl', 'vm.drop_caches=1']
        return ['echo', '1', '>', '/proc/sys/vm/drop_caches']


    def dentries_inodes_cache(self, arg):
        """Method returns a list of commands to drop dentries and inodes cache
using systemd"""
        if '-sysd' in arg:
            ## user wants to use systemd
            return ['sudo', 'sysctl', 'vm.drop_caches=2']
        return ['echo', '2', '>', '/proc/sys/vm/drop_caches']

    def clear_all_caches(self, arg):
        """Method returns a list of commands to drop all caches using
systemd"""
        if '-sysd' in arg:
            ## user wants to use systemd
            return ['sudo', 'sysctl', 'vm.drop_caches=3']
        return ['echo', '3', '>', '/proc/sys/vm/drop_caches']


    def run(self, cache):
        """This method will run the commands to drop the caches."""
        with Popen(cache, stderr=PIPE, stdout=PIPE) as proc:
            drop = proc.communicate()

        if drop[0] != '':
            return drop[0].decode()
        return drop[1].decode()


    def clear_cache(self, arg):
        """This method will run through the users arguments to find which
caches to clear."""

        ## we need to create some flag variables so we can check if they
        ## were passed as arguments.
        pages = ('-pc', '--pages')
        inode = ('-di', '--inode')
        all_caches = ('-a', '--all')

        print('Clearing cache...')

        ## ask if the user wants to drop page cache
        if 1 in (1 for x in arg if x in pages):
            self.run(self.page_cache(arg))

        ## ask if the user wants to drop dentries and inodes cache's
        if 1 in (1 for x in arg if x in inode):
            self.run(self.dentries_inodes_cache(arg))

        ## ask if the user wants to drop all cache's
        if 1 in (1 for x in arg if x in all_caches):
            self.run(self.clear_all_caches(arg))

        print('Cache has been cleared.')
        self.cache_info(arg, True)


    def cache_info(self, arg:list, refresh:bool=False):
        """Print some cache info to terminal."""
        gib = 1000000000
        prnt = ('-s', '--show')

        if 1 in (1 for x in arg if x in prnt):
            if refresh:
                self.memory = vm()

            total = self.memory.total / gib
            total = f"  Total memory     : {total} Gib"

            used = self.memory.used / gib
            used = f"  Used memory      : {used} Gib"

            percent = self.memory.percent
            percent = f"  Percent used     : {percent}%"

            cached = self.memory.cached / gib
            cached = f"  Cached memory    : {cached} Gib"

            shared = self.memory.shared / gib
            shared = f"  Shared memory    : {shared} Gib"

            available = self.memory.available / gib
            available = f"  Available memory : {available} Gib"

            free = self.memory.free / gib
            free = f"  Free memory      : {free} Gib"

            memory = [total, used, percent, cached, shared, available, free]

            for mem in memory:
                print(mem)


if __name__ == '__main__':

    sep = (':', '=')
    args = [x for x in argv[1:] for y in sep if not y in x]
    kargs = {x.split(y, 1)[0]: x.split(y, 1)[1] for x in argv[1:]
             for y in sep if y in x}

    if not args and not kargs:
        Memory(*['-s'])
    elif 1 in (1 for x in args if x in ('-h', '--help')):
        print(__doc__)
    else:
        Memory(*args, **kargs)
