This file is to show and manage dentries, inodes, and page caches/memory on
linux operating systems.

## Useful links:

https://stackoverflow.com/questions/29870068/what-are-pagecache-dentries-inodes

https://en.wikipedia.org/wiki/Page_cache

https://linuxize.com/post/cron-jobs-every-5-10-15-minutes/

Inspired by:  https://lemmy.ml/post/14918974

## Usage:

  ### Help:

    -h, --help     |  Show this info

  ### Checking memory flag:

    -s, --show     |  Passed in args will print useful memory info. Can be
                   |  passed with --clear flag to show info before and after
                   |  caches are cleared.

  ### Clearing memory flags and keyword arguments:

    -c, --clear    |   Will activate the clearing of caches. Everything further
                       indented below will require this flag to operate.

      -pc, --pages |   Will clear pages cache.

      -id, --inode |   Will clear the dentries and inodes cache.

      -a, --all    |   Will clear all the caches. When passed the two above
                       flags are not needed.

        -sysd      |   Will use systemd to clear the caches. If passed the 
                   |   above 3 flags will use systemd to clear caches. If not
                   |   passed program will use non systemd method.

      Keyword arguments:

        percent    |   Any number between 1 and 100. Will only clear the caches
                   |   when the total used memory percentage is greater than
                   |   user defined percentage defined by this argument.
                   |   Default is 99.

        apps       |   A list of apps separated by commas without spaces. The
                   |   program will look through all running processes to find
                   |   any process in the list. If found and the desired memory
                   |   percentage is exceeded the desired caches will be
                   |   cleared.

### Examples:

    Show info:
        python memoryfix.py --show

    Show info, clear all caches using systemd:
        python memoryfix.py -s --clear -all -sysd

    Clear dentries and inodes caches when default memory value has been
    exceeded and the users specified applications have been found to be running
    on the system:

        python memoryfix.py -c -id apps=chrome,blender,vlc

    Clear page cache when used memory exceeds 87%:
        python memoryfix.py -c percent=87

            **Note: remember there must not be any spaces in keywords,
                    separators, and arguments.

### Cronjob setup for every five minutes and clear cache only if used memory is above 95 percent:

    Download file. Move it to wherever you want. My case ~/.scripts .
    Make file executable:
        chmod +x memoryfix.py

    For security might want to make file owned by root so it cant be edited.
        sudo chown root:root memoryfix.py

    Set the crontab:
        sudo EDITOR=nano crontab -e

    */5 * * * * /usr/bin/python /home/$USER/.scripts/memory.py -c percent=95

    Place the above line in the nano editor replacing the $USER with your
    username and adjust for where you have the file located. Now save the 
    crontab:
              [Ctrl] + o
              [Ctrl] + m
              [Ctrl] + x
